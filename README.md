# This is a public archive of my GitLab account. I have since moved back to Github to make project discovery and contribution easier.

## [View my Github account here](https://github.com/jvadair)
I <3 GitLab


# Ciao!

My name is James. I make lots of web services and command-line tools. Most of my projects were created to learn or play around with a specific concept or module, but some still receive updates.

Languages I can write in:
1. Python
2. C++
3. Java
4. HTML
5. CSS
6. Visual Basic

---

Favorite project: [pyndb](https://gitlab.com/jvadair/pyndb)

Other hobbies include: reading, 3d printing, camping

My primary spoken language is English, but I can also speak un po' d'Italiano.
